# What?

A simple email alert system for suricata.

# How it works

suricatalert watches the /var/log/suricata/fast.log file for write changes, upon noticing a change it checks to see what the level of the reported indicident is, and if it is a high enough level it sends an email alert to the configured email address. If it's too low, it records the information to /var/log/suricatalert.log, just so you can be certain it saw the event and chose to ignore it.

I currently use this in my homelab, I wanted something simple and suricata doesn't seem (from what I could tell) to have alerting built into it, so this was a solution I strung together in a couple of spare hours. I've been using it without fuss for a few months so it's about time it makes it onto git.

## TODO

```
enable true multi-To: handling
openrc script
apkbuild
better internal logging
more secure smtp auth
```

## Installation

```
mkdir -p /etc/suricatalert
cp src/example-config.yaml /etc/suricatalert/
touch /var/log/suricatalert.log
go build src/main.go
mv src/suricatalert /usr/local/bin

Then add the following to crontab:
@reboot						sleep 5 && /usr/local/bin/suricatalert >> /var/log/suricatalert.log
```
