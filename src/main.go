package main

import (
	"io"
	"os"
	"log"
	"fmt"
	"time"
	"regexp"
	"net/smtp"
	"gopkg.in/yaml.v2"
	"github.com/fsnotify/fsnotify"
)

type Config struct {
	From string `yaml:"From"`
	AppPass string `yaml:"AppPass"`
	To []string `yaml:"To"`
	SmtpHost string `yaml:"SmtpHost"`
	SmtpPort string `yaml:"SmtpPort"`
}

var cfg Config

func parseConfig(cfg *Config, file string) {
	f, err := os.Open(file)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		log.Fatalln(err)
	}
}

func sendmail(alert string) {
	curtime := time.Now()
	ts := curtime.Format(time.RFC1123Z)
	//TODO: for to in cfg.To concat To: value
	msg := "To: " + cfg.To[0] + "\r\n" + "Subject: [IDS Alert] " + ts + "\r\n" + "\r\n" + alert
	message := []byte(msg)
	auth := smtp.PlainAuth("", cfg.From, cfg.AppPass, cfg.SmtpHost)
	err := smtp.SendMail(cfg.SmtpHost+":"+cfg.SmtpPort, auth, cfg.From, cfg.To, message)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println("Alert sent!")
}

func critlvl(alert string) {
	match, _ := regexp.MatchString("Priority: 1", alert)

	fmt.Println(match, alert)
	if match == true {
		sendmail(alert)
	} else {
		log.Println("Alert priority not high enough to report: " + alert)
	}
}

func getLastLineWithSeek(filepath string) string {
    fileHandle, err := os.Open(filepath)

    if err != nil {
        panic("Cannot open file")
        os.Exit(1)
    }
    defer fileHandle.Close()

    line := ""
    var cursor int64 = 0
    stat, _ := fileHandle.Stat()
    filesize := stat.Size()
    for { 
        cursor -= 1
        fileHandle.Seek(cursor, io.SeekEnd)

        char := make([]byte, 1)
        fileHandle.Read(char)

        if cursor != -1 && (char[0] == 10 || char[0] == 13) { // stop if we find a line
            break
        }

        line = fmt.Sprintf("%s%s", string(char), line) // there is more efficient way

        if cursor == -filesize { // stop if we are at the begining
            break
        }
    }
    return line
}


func main() {
	parseConfig(&cfg, "/etc/suricatalert/config.yaml")
	
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal("NewWatcher failed: ", err)
	}
	defer watcher.Close()

	done := make(chan bool)
	go func() {
		defer close(done)

		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				log.Printf("%s %s\n", event.Name, event.Op)
				critlvl(getLastLineWithSeek("/var/log/suricata/fast.log"))
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}

	}()

	err = watcher.Add("/var/log/suricata/fast.log")
	if err != nil {
		log.Fatal("Add failed:", err)
	}
	<-done
}
